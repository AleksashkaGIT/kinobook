<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Searchb extends MY_Controller{
	public function __construct(){
		parent:: __construct();
	}

	public function index(){
		$this->data['title'] = "Поиск книг";
		$this->load->model('search_model');
		$this->data['search_result_book']=array();
		
		if($this->input->get('q_search_book')){
			$this->data['search_result_book']=$this->search_model->searchb($this->input->get('q_search_book'));
		}
		
		$this->load->view('templates/header',$this->data);
		$this->load->view('searchb',$this->data);
		$this->load->view('templates/footer');
	}
}