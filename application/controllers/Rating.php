<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Rating extends MY_Controller{
	public function __construct(){
		parent:: __construct();
	}

	public function index(){
		$this->data['title'] = "Рейтинг";
		$this->load->model('films_model');
		$this->data['movie_rating']=$this->films_model->getFilmsByRating(10);
		$this->load->model('books_model');
		$this->data['book_rating']=$this->books_model->getBooksByRating(10);
		
		$this->load->view('templates/header',$this->data);
		$this->load->view('rating/index',$this->data);
		$this->load->view('templates/footer');
	}
}