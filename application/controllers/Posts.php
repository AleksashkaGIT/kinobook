<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller{

	public function __construct(){
		parent:: __construct();
		$this->load->model('posts_model');
	} 

	public function index(){
		$this->data['title'] = "Все посты";
		$this->data['posts'] = $this->posts_model->getPosts();

		$this->load->view('templates/header',$this->data);
		$this->load->view('posts/index',$this->data);
		$this->load->view('templates/footer');
	}

	public function view($slug = NULL){
		$this->data['posts_item']=$this->posts_model->getPosts($slug);

		if(empty($this->data['posts_item'])){
			show_404();
		}

		$this->data['title'] = $this->data['posts_item']['title'];
		$this->data['content'] = $this->data['posts_item']['text'];

		$this->load->view('templates/header',$this->data);
		$this->load->view('posts/view',$this->data);
		$this->load->view('templates/footer');
	}

	public function create(){

		if(!$this->dx_auth->is_admin()){
			//show_404();
			$this->load->helper('url_helper');
			redirect('/', 'location');
		}

		$this->data['title'] = "добавить пост";

		if($this->input->post('slug') && $this->input->post('title') && $this->input->post('text')){
			$slug = $this->input->post('slug');
			$title = $this->input->post('title');
			$text = $this->input->post('text');
			if($this->posts_model->setPosts($slug, $title, $text)){
				$this->load->view('templates/header',$this->data);
				$this->load->view('posts/success',$this->data);
				$this->load->view('templates/footer');
			}
		}else{
			$this->load->view('templates/header',$this->data);
			$this->load->view('posts/create',$this->data);
			$this->load->view('templates/footer');
		}
	}

	public function edit($slug = NULL) {

		if(!$this->dx_auth->is_admin()){
			//show_404();
			$this->load->helper('url_helper');
			redirect('/', 'location');
		}

		$this->data['title'] = "редактировать пост";
		$this->data['posts_item'] = $this->posts_model->getPosts($slug);
		$this->data['title_posts'] = $this->data['posts_item']['title'];
		$this->data['content_posts'] = $this->data['posts_item']['text'];
		$this->data['slug_posts'] = $this->data['posts_item']['slug'];

		if($this->input->post('slug') && $this->input->post('title') && $this->input->post('text')) {
			$slug = $this->input->post('slug');
			$title = $this->input->post('title');
			$text = $this->input->post('text');

			if($this->posts_model->updatePosts($slug, $title, $text)) {
				echo "Пост успешно отредактирован";
			}
		}
		$this->load->view('templates/header', $this->data);
		$this->load->view('posts/edit', $this->data);
		$this->load->view('templates/footer');
	}

	public function delete($slug=NULL){

		if(!$this->dx_auth->is_admin()){
			//show_404();
			$this->load->helper('url_helper');
			redirect('/', 'location');
		}

		$this->data['posts_delete'] = $this->posts_model->getPosts($slug);

		if(empty($this->data['posts'])) {
			show_404();
		}

		$this->data['title'] = "удалить пост";
		$this->data['result'] = "ошибка удаления";

		if($this->posts_model->deletePosts($slug)){
			$this->data['result'] = "Пост успешно удален";
		}

		$this->load->view('templates/header', $this->data);
		$this->load->view('posts/delete', $this->data);
		$this->load->view('templates/footer');
	}
}