<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends MY_Controller{
	public function __construct(){
		parent:: __construct();
	}

	public function index(){
		$this->data['title'] = "Главная страница";
		$this->load->model('films_model');
		$this->data['movie']=$this->films_model->getFilms(false, 8);
		$this->load->model('books_model');
		$this->data['book']=$this->books_model->getBooks(false, 8);

		$this->load->view('templates/header',$this->data);
		$this->load->view('main/index',$this->data);
		$this->load->view('templates/footer');
	}

	public function contacts(){
		$this->data['title'] = "Контакты";
		if($this->input->post('message', 'username', 'email')){
			$this->load->library('email');
			$this->email->from('email', 'username');
			$this->email->to('someone@example.com');
			$this->email->message('message');
			$this->email->send();
		}

		$this->load->view('templates/header',$this->data);
		$this->load->view('main/contacts',$this->data);
		$this->load->view('templates/footer');
	}

	public function send(){
		$this->load->view('templates/header',$this->data);
		$this->load->view('main/send',$this->data);
		$this->load->view('templates/footer');
	}
}