<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Books extends MY_Controller{
	public function __construct(){
		parent:: __construct();
	}

	public function view($slug = NULL){
		$book_slug = $this->books_model->getBooks($slug, false);

		if(empty($book_slug)) {
			show_404();
		}

		$this->load->model('comments_model');
		$this->data['comments_book'] = $this->comments_model->getCommentsBook($book_slug['id'], 100);
		
		if($this->input->post('comment_text')){
			$comment_text = $this->input->post('comment_text');
			$user_id = $this->dx_auth->get_user_id();
			$this->comments_model->setCommentsBook($book_slug['id'], $user_id, $comment_text);
			header("Location:/books/type/books/"); 
		}

		$this->data['title_book'] = $book_slug['name'];
		$this->data['year_book'] = $book_slug['year'];
		$this->data['rating_book'] = $book_slug['rating'];
		$this->data['author_book'] = $book_slug['author'];
		$this->data['poster_book'] = $book_slug['poster'];
		$this->data['description_book'] = $book_slug['descriptions'];
		$this->data['read_code'] = $book_slug['read_code'];

		$this->load->view('templates/header',$this->data);
		$this->load->view('books/view',$this->data);
		$this->load->view('templates/footer');
	}

	public function type($slug = NULL){
		$this->data['book_data'] = null;

		if($slug == "books"){
			$this->data['title'] = "Книги";
			$this->data['book_data'] = $this->books_model->getBooks(false, 10);
		}

		if($this->data['book_data'] == null){
			show_404();
		}
		
		$this->load->view('templates/header',$this->data);
		$this->load->view('books/type',$this->data);
		$this->load->view('templates/footer');
	}
}