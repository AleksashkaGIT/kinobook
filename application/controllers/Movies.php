<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Movies extends MY_Controller{
	public function __construct(){
		parent:: __construct();
	}

	public function view($slug = NULL){
		$movie_slug = $this->films_model->getFilms($slug, false);

		if(empty($movie_slug)) {
			show_404();
		}

		$this->load->model('comments_model');
		$this->data['comments'] = $this->comments_model->getComments($movie_slug['id'], 100);
		
		if($this->input->post('comment_text')){
			$comment_text = $this->input->post('comment_text');
			$user_id = $this->dx_auth->get_user_id();
			$this->comments_model->setComments($movie_slug['id'], $user_id, $comment_text);
			header("Location:/movies/type/films/"); 
		}
		
		$this->data['title'] = $movie_slug['name'];
		$this->data['year'] = $movie_slug['year'];
		$this->data['rating'] = $movie_slug['rating'];
		$this->data['director'] = $movie_slug['director'];
		$this->data['description_movie'] = $movie_slug['descriptions'];
		$this->data['player_code'] = $movie_slug['player_code'];

		$this->load->view('templates/header',$this->data);
		$this->load->view('movies/view',$this->data);
		$this->load->view('templates/footer');
	}

	public function type($slug = NULL){
		$this->data['movie_data'] = null;

		if($slug == "films"){
			$this->data['title'] = "Экранизации книг";
			$this->data['movie_data'] = $this->films_model->getFilms(false, 10);
		}

		if($this->data['movie_data'] == null){
			show_404();
		}
		
		$this->load->view('templates/header',$this->data);
		$this->load->view('movies/type',$this->data);
		$this->load->view('templates/footer');
	}
}