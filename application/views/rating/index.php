<h1>Рейтинг экранизаций</h1>
<hr>
  <table class="table table-striped">
   <thead>
    <tr>
      <th></th>
      <th class="col-lg-4 col-md-4 col-sm-4 col-xs-2">Фильмы</th>
      <th class="text-left col-lg-3 col-md-3 col-sm-3 col-xs-2">Год</th>
      <th class="text-left col-lg-4 col-md-4 col-sm-4 col-xs-2">Рейтинг</th>
    </tr>
  </thead>
</table>

<?php foreach ($movie_rating as $key => $value): ?>
  <table class="table table-striped">
    <tbody>
      <tr>
        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
          <a href="/movies/view/<?php echo $value['slug']; ?>/"><img class="img-responsive img-thumbnail" src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>"></a>
        </td>
        <td class="vert-align text-left col-lg-3 col-md-3 col-sm-3 col-xs-2"><a href="/movies/view/<?php echo $value['slug']; ?>/"><?php echo $value['name']; ?></a></td>
        <td class="text-left vert-align col-lg-3 col-md-3 col-sm-3 col-xs-2"><?php echo $value['year']; ?></td>
        <td class="text-left vert-align col-lg-3 col-md-3 col-sm-3 col-xs-2"><span class="badge"><?php echo $value['rating']; ?></span></td>
      </tr>
    </tbody>
</table>
<div class="margin-8"></div>
<?php endforeach ?>

<h1>Рейтинг книг</h1>
<hr>
<table class="table table-striped">
  <thead>
   <tr>
      <th></th>
      <th class="text-left col-lg-4 col-md-4 col-sm-4 col-xs-2">Книга</th>
      <th class="text-left col-lg-3 col-md-3 col-sm-3 col-xs-2">Год</th>
      <th class="text-left col-lg-3 col-md-3 col-sm-3 col-xs-2">Рейтинг</th>
    </tr>
  </thead>
</table>
<?php foreach ($book_rating as $key => $value): ?>
  <table class="table table-striped">
  <tbody>
      <tr>
        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-2">
          <a href="/books/view/<?php echo $value['slug']; ?>/"><img class="img-responsive img-thumbnail" src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>"></a>
        </td>
        <td class="vert-align text-center"><a href="/books/view/<?php echo $value['slug']; ?>/">"<?php echo $value['name']; ?>"<?php echo $value['author']; ?></a></td>
        <td class="text-left vert-align col-lg-3 col-md-3 col-sm-3 col-xs-2"><?php echo $value['year']; ?></td>
        <td class="text-left vert-align col-lg-3 col-md-3 col-sm-3 col-xs-2"><span class="badge"><?php echo $value['rating']; ?></span></td>
      </tr>
    </tbody>
  </table>   
<?php endforeach ?>