<h1><?php echo $title ?></h1>
<hr>
<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="<?php echo $player_code ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</div>
<div class="well info-block text-center">
  Год: <span class="badge"><?php echo $year ?></span>
  Рейтинг: <span class="badge"><?php echo $rating ?></span>
  Режиссёр: <span class="badge"><?php echo $director ?></span>
</div>
<div class="margin-8"></div>

<h2>Описание <?php echo $title ?></h2>
<div class="well">
 <?php echo $description_movie ?>
</div>
<div class="margin-8"></div>

<h2>Отзывы о фильме <?php echo $title ?></h2>
<hr>
<?php foreach ($comments as $key => $value): ?>
  <div class="panel panel-info">
    <div class="panel-heading"><i class="glyphicon glyphicon-user"></i><span> <?php echo getUserNameById($value['user_id'])->username; ?> </span></div>
    <div class="panel-body">
     <?php echo $value['comment_text']; ?>
    </div>
  </div>
<?php endforeach ?>

<?php if ($this->dx_auth->is_logged_in()): ?>
  <form method="post">
    <div class="form-group">
      <textarea class="form-control" type = "input" name="comment_text" placeholder="введите текст комментария"></textarea>
    </div>
    <button class="btn btn-lg btn-warning">Отправить</button>
  </form>
<?php endif ?>
<div class="margin-8"></div>

