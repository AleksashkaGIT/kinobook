<div class="row">
  <div class="well clearfix">
    <div class="col-lg-3 col-md-2 text-center">
      <img class="img-thumbnail" src="<?php echo $poster_book ?>" alt="<?php echo $title_book ?>">
        <p><?php echo $author_book ?></p>
        <p><?php echo $title_book ?></p>
        Год: <span class="badge"><?php echo $year_book ?></span>
        Рейтинг: <span class="badge"><?php echo $rating_book ?></span>
    </div>
    <div class="col-lg-9 col-md-10">
      <h2>Описание книги "<?php echo $title_book ?>"</h2>
      <p>
        <?php echo $description_book ?>
      </p>
    </div>
    <div class="col-lg-12">
      <a href="<?php echo $read_code ?>" class="btn btn-lg btn-warning pull-right">Читать онлайн</a>
    </div>
  </div>
</div>
<div class="margin-8"></div>

<h2>Отзывы о книге "<?php echo $title_book ?>"</h2>
<hr>
<?php foreach ($comments_book as $key => $value): ?>
<div class="panel panel-info">
  <div class="panel-heading"><i class="glyphicon glyphicon-user"></i><span><?php echo getUserNameById($value['user_id'])->username; ?></span></div>
  <div class="panel-body">
   <?php echo $value['comment_text']; ?>
  </div>
</div>
<?php endforeach ?>

<?php if ($this->dx_auth->is_logged_in()): ?>
  <form method="post">
    <div class="form-group">
      <textarea class="form-control" type = "input" name="comment_text" placeholder="введите текст комментария"></textarea>
    </div>
    <button class="btn btn-lg btn-warning">Отправить</button>
  </form>
<?php endif ?>
<div class="margin-8"></div>