<h2>Экранизации книг</h2>
<hr>
<div class="row">
  <?php foreach ($movie as $key => $value): ?>
    <div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
      <a href="/movies/view/<?php echo $value['slug'] ?>/"><img src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>">  
      <div class="films_label"><?php echo $value['name']; ?></div></a>
  </div>
  <?php endforeach ?>
</div>
<div class="margin-8"></div>

<h2>Популярные книги</h2>
<hr>
<div class="row">
  <?php foreach ($book as $key => $value): ?>
    <div class="films_block col-lg-3 col-md-3 col-sm-3 col-xs-6">
      <a href="/books/view/<?php echo $value['slug'] ?>/"><img src="<?php echo $value['poster']; ?>" alt="<?php echo $value['name']; ?>">
      <div class="films_label"><?php echo $value['name']; ?></div></a>    
    </div>   
  <?php endforeach ?>
</div>
<div class="margin-8"></div>

<a href="/posts/view/post-1"><h3>Как снимали Интерстеллар</h3></a>
<hr>
<p>45 лет исполнилось Кристоферу Нолану — одному из самых успешных режиссеров нашего времени, чьи работы одинаково любимы как взыскательными критиками, так и простыми зрителями. Фильмом изначально занималась студия Paramount. Когда Кристофер Нолан занял место режиссера, студия Warner Bros., которая выпускала его последние фильмы, добилась участия в проекте.</p>
<a href="/posts/view/post-1" class="btn btn-danger pull-right">читать</a>
<div class="margin-8 clear"></div>
<a href="/posts/view/post-2"><h3>Интересные факты о романе "451 градус по Фаренгейту"</h3></a>
<hr>
<p>1. Роман был назван «451 градус по Фарингейту» потому, что огромное внимание в нем уделяется сожжению книг, при этом температура воспламенения бумаги составляет 450 градусов. По признанию автора небольшая ошибка была сделана из-за того, что пожарный, с которым консультировался Брэдбери перед написанием бестселлера, попросту спутал температурные шкалы.</p>
<a href="/posts/view/post-2" class="btn btn-danger pull-right">читать</a>
<div class="margin-8 clear"></div>