<h1><?php echo $title; ?></h1>
<hr>
<h4>Отправьте Ваш отзыв о портале KinoBook</h4>
<form method="post" action="/main/send">
  <div class="form-group">
    <input type="text" placeholder="Ваше имя" name="username" class="form-control input-lg">
  </div>
  <div class="form-group">
    <input type="text" placeholder="Ваш e-mail" name="email" class="form-control input-lg">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="message" ></textarea>
  </div>
  <button class="btn btn-lg btn-warning pull-right">Отправить</button>
</form>