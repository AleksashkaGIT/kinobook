<?php 
class Films_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function getFilms($slug = FALSE, $limit){
		if($slug == FALSE){
			$query = $this->db
				->order_by('rating', 'desc')
				->limit($limit)
				->get('movie');
			return $query->result_array();
		}
		$query = $this->db->get_where('movie', array('slug'=> $slug));
		return $query->row_array();
	}

	public function getFilmsByRating($limit){
		$query = $this->db
			->order_by('rating', 'desc')
			->where('rating >', 0)
			->limit($limit)
			->get('movie');
		return $query->result_array();
	}
}
