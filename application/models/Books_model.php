<?php 
class Books_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function getBooks($slug = FALSE, $limit){
		if($slug == FALSE){
			$query = $this->db
				->order_by('rating', 'desc')
				->limit($limit)
				->get('books');
			return $query->result_array();
		}
		$query = $this->db->get_where('books', array('slug'=>$slug));
		return $query->row_array();
	}

	public function getBooksByRating($limit){
		$query = $this->db
			->order_by('rating', 'desc')
			->where('rating >', 0)
			->limit($limit)
			->get('books');
		return $query->result_array();
	}

	public function getBooksOnPage($row_count, $offset){
		$query = $this->db
			->order_by('rating', 'desc')
			->get('books', $row_count, $offset);
		return $query->result_array();
	}
}