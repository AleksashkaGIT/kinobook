<?php 
class Search_model extends CI_Model{

	public function __construct() {
	$this->load->database();
	}

	public function search($q){
		$array_search = array(
			'name' => $q,
			'descriptions' => $q
		);
		$query = $this->db
			->or_like($array_search)
			->limit(100)
			->get('movie');
		return $query->result_array();
	}

	public function searchb($q){
		$array_search_book = array(
			'name' => $q,
			'descriptions' => $q
		);
		$query = $this->db
			->or_like($array_search_book)
			->limit(100)
			->get('books');
		return $query->result_array();
	}
}