<?php 

class Comments_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function getComments($movie_id, $limit) {
		$query = $this->db
			->where('movie_id', $movie_id)
			->limit($limit)
			->get('comments');
		return $query->result_array();	
	}

	public function setComments($movie_id, $user_id, $comment_text){
		$data = array(
			'movie_id' => $movie_id,
			'user_id' => $user_id,
			'comment_text' => $comment_text
		);
		return $this->db->insert('comments', $data);
	}

	public function getCommentsBook($books_id, $limit) {
		$query = $this->db
			->where('books_id', $books_id)
			->limit($limit)
			->get('comments_book');
		return $query->result_array();	
	}

	public function setCommentsBook($books_id, $user_id, $comment_text){
		$data = array(
			'books_id' => $books_id,
			'user_id' => $user_id,
			'comment_text' => $comment_text
		);
		return $this->db->insert('comments_book', $data);
	}
}